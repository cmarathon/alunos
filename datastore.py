from pymongo import MongoClient

def get_db():
    client = MongoClient('localhost:27017')
    db = client.slides
    return db

def add_slide(db, dic, key):
    slide = get_slide(db, key, dic[key]) 
    if slide:
        db.slides.delete_one(slide)
    db.slides.insert(dic)

def add_slides(db, dic_list, key):
    for dic in dic_list:
        add_slide(db, dic, key)

def get_slide(db, key, value):
    return db.slides.find_one({key : value})

def get_slides(db):
    return db.slides.find_one()
    
