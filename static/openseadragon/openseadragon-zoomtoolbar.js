/*const $ = OpenSeadragon;
if (!$.version || $.version.major < 2) {
  throw new Error(
    'This version of OpenSeadragon Guides requires OpenSeadragon version 2.0.0+'
  );
}*/
(function ($) {
    'use strict';

    if (!$.version || $.version.major < 2) {
        throw new Error('This version of OpenSeadragon Zoomtoolbar requires OpenSeadragon version 2.0.0+');
    }

	// Add new instance of zoomtoolbar plugin to the viewers prototype
	$.Viewer.prototype.Zoom = function(options) {
	  if (!this.zoomInstance || options) {
		options = options || {};
		options.viewer = this;
		this.zoomInstance = new $.Zoom(options);
	  }
	  return this.zoomInstance;
	};

	//-----
	$.Zoom = function(options) {
		$.extend(true, this, {
			// internal state properties
			viewer: null,

			// Default options
			//ZoomButton: null,
			prefixUrl: "/static/openseadragon/zoomtoolbarImages/",

		/*	navImages: {
				zoomtool: {
					REST:	'zoomtool_rest_long.png',
		    			GROUP:	'zoomtool_grouphover_long.png',
		    			HOVER:	'zoomtool_hover.png',
		    			DOWN:	'zoomtool_pressed.png'
		 		}
			}*/
	  	}, options);

		// Add plugin buttons to the viewer
		//$.extend(true, this.navImages, this.viewer.navImages);

		// Openseadragon button behaviour, make sure the plugin's buttons act the
		// same as the default buttons from the viewer
		const prefix = this.prefixUrl || this.viewer.prefixUrl || '';



		const useGroup = this.viewer.buttons && this.viewer.buttons.buttons;
		//const anyButton = useGroup ? this.viewer.buttons.buttons[0] : null;
		//const onFocus = anyButton ? anyButton.onFocus : null;
		//const onBlur = anyButton ? anyButton.onBlur : null;

		this.ZoomButton = new $.Button({
		//element: this.ZoomButton ? $.getElement(this.ZoomButton) : null,
		element: document.getElementById('zoom'),
		tooltip: $.getString('Tooltips.ZoomTool') || 'Zoom INFO',
		/*clickTimeThreshold: this.viewer.clickTimeThreshold,
		clickDistThreshold: this.viewer.clickDistThreshold,

		srcRest: prefix + this.navImages.zoomtool.REST,
		srcGroup: prefix + this.navImages.zoomtool.GROUP,
		srcHover: prefix + this.navImages.zoomtool.REST,//HOVER,
		srcDown: prefix + this.navImages.zoomtool.REST//DOWN,
		*/
		});
	
		if (useGroup) {
			//this.viewer.buttons.buttons.push(this.ZoomButton);
			this.viewer.buttons.element.appendChild(this.ZoomButton.element);
		}
	};
})(OpenSeadragon);

