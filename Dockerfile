# Use an official Python runtime as a parent image
FROM python:2.7
#FROM ubuntu:17.10

# let the container know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

RUN pip install -r requirements.txt

# Run app.py when the container launches
CMD ["python", "alunos.py"]

