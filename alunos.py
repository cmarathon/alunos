from flask import Flask, render_template, abort, request
import datastore as ds

app = Flask(__name__)


@app.route('/alunos/<user>/<oid>/')
def alunos(user, oid):
	try: 
		db = ds.get_db()
		slide = db.slides.find_one({'image_id':oid})	
		kwargs = {}
		kwargs['debug'] = "false"
		kwargs['slide_id'] = oid 
		kwargs['user_name'] = user 
		kwargs['line1'] = slide['line_1'] if slide else 'line1'
		kwargs['line2'] = slide['line_2'] if slide else 'line2'

		return render_template('slide.html', **kwargs)

	except Exception as e: 
		return e.message

@app.route('/', defaults={'path': ''}, methods=['POST'])
@app.route('/<path:path>', methods=['POST'])
def alunos_moodle(path):
	kwargs = {}
	if request.form.get('action') == 'BioPat':
		db = ds.get_db()
		kwargs['slide_id'] = request.form.get('object_id')
		slide = db.slides.find_one({'image_id':kwargs['slide_id']})	
		kwargs['debug'] = "false"
		kwargs['line1'] = slide['line_1'] if slide else ''
		kwargs['line2'] = slide['line_2'] if slide else ''
		kwargs['user_name'] = request.form.get('user').split('<')[0]
		return render_template('slide.html', **kwargs)
	else:
		return "Access Denied!"

 
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
